module gitlab.com/greut/eclint

go 1.20

require (
	github.com/editorconfig/editorconfig-core-go/v2 v2.6.2
	github.com/go-logr/logr v1.2.0
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f
	github.com/google/go-cmp v0.6.0
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mattn/go-colorable v0.1.13
	golang.org/x/term v0.18.0
	golang.org/x/text v0.14.0
	k8s.io/klog/v2 v2.100.1
)

require (
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/mod v0.16.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
